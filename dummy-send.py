#!/usr/bin/env python
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

channel = connection.channel()

channel.exchange_declare(exchange='command_queue', type='fanout')

channel.basic_publish(exchange='command_queue',
                      routing_key='',
                      body='Hello, world!')
connection.close()
