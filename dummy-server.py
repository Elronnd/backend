import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

channel = connection.channel()

channel.exchange_declare(exchange='logs', type='fanout')

queue_name = channel.queue_declare()
channel.queue_bind(exchange='msg_queue', queue=queue_name.method.queue)

channel.basic_consume(lambda _, __, ___, body: print(body), queue=queue_name.method.queue, no_ack=True)

channel.start_consuming()
