from __future__ import unicode_literals, print_function, division

import pika
import sys
import datetime
import json
from uuid import uuid4 as uuid
from random import randrange

from twisted.internet import defer, endpoints, protocol, reactor, task
from twisted.python import log
from twisted.words.protocols import irc

# need to inherit from object for @property, because python2 is shit
class IRCConnector(irc.IRCClient, object):
#    nickname = "Ircbotconnectorthingymagobber"
#    def __init__(self, nick, realname=None, authuser=None, authpass=None):
    def __init__(self):
        self.deferred = defer.Deferred()
        self._nick = "IRC_BOT_no_" + str(randrange(7000))

    @property
    def nickname(self):
        return self._nick
        # return self.factory.nickname
    @nickname.setter
    def nickname(self, newval):
        self._nick = newval

#    @property
#    def username(self):
#        return self.factory.username
#    @property
#    def password(self):
#        return self.factory.password
#    @property
#    def realname(self):
#        return self.factory.realname

    def connectionLost(self, reason):
        self.deferred.errback(reason)

    def signedOn(self):
        for channel in self.factory.channels:
            self.join(channel)

    def privmsg(self, user, channel, message):
        nick, _, host = user.partition('!')
        self.factory.handler.post(nick, host, channel, message.strip(), self.factory.guid)

    def _showError(self, failure):
        return failure.getErrorMessage()


class Msghandler:
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        self.channel = self.connection.channel()
        # I'm REALLY good at thinking of names
        self.channel.exchange_declare(exchange='msg_queue', type='fanout')
        # seriously, though, it's not THAT bad.  I mean, it's a queue for MESSAGES from irc

    def post(self, nick, host, channel, message, guid):
        # Double {{ means that it'll be treated as a literal }} instead of as a formatter by the formatting function
        self.channel.basic_publish(exchange='msg_queue', routing_key='', body='{{"date":"{}","nick":"{}","host":"{}","channel":"{}","msg":"{}","guid":"{}"}}'.format(datetime.datetime.now(), nick, host, channel, message, guid))


class IRCConnectorFactory(protocol.ReconnectingClientFactory):
    protocol = IRCConnector
    channels = ['#mytesting']

    def __init__(self, handler, guid):
        self.handler = handler
        self.guid = guid

class Twistedmaster(object):
    def __init__(self):
        self.connections = dict()
        log.startLogging(sys.stderr)
        self.handler = Msghandler()

        cc = protocol.ClientCreator(reactor, pika.adapters.twisted_connection.TwistedProtocolConnection, pika.ConnectionParameters())
        d = cc.connectTCP('localhost', 5672)
        d.addCallback(lambda protocol: protocol.ready)
        d.addCallback(self.listen)

        task.react(self.reactorun, ('tcp:irc.freenode.net:6667', self.handler, str(uuid()), 'IrcBotTestingThingy'))
        reactor.run()


    def reactorun(self, reactor, server, handler, guid, nick, realname=None, authuser=None, authpass=None):
        endpoint = endpoints.clientFromString(reactor, server)
        factory = IRCConnectorFactory(handler, guid)
        d = endpoint.connect(factory)
        d.addCallback(lambda protocol: protocol.deferred)

        self.connections[guid] = d
        return d

    @defer.inlineCallbacks
    def listen(self, connection):
        @defer.inlineCallbacks
        def listened(queue_obj):
            ch, method, properties, body = yield queue_obj.get()

            if body: print(body)


        channel = yield connection.channel()
        exchange = yield channel.exchange_declare(exchange='command_queue', type='fanout')

        queue = yield channel.queue_declare()

        yield channel.queue_bind(exchange='command_queue', queue=queue.method.queue)

        queue_object, consumer_tag = yield channel.basic_consume(queue=queue.method.queue, no_ack=True)

        l = task.LoopingCall(listened, queue_object)

        l.start(0.01)


    def addconnection(self, guid, server, nick, realname=None, authuser=None, authpass=None, usessl=False, port=6667):
        if usessl:
            raise NotImplementedError("SSL is not supported")


if __name__ == '__main__':
    Twistedmaster()
